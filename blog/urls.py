from django.urls import path

from blog.views import contacts, ShowNewsView, NewsDetailView, CreateNewsView, UpdateNewsView, DeleteNewsView, \
    UserAllNewsView

app_name = 'blog'

urlpatterns = [
    path('', ShowNewsView.as_view(), name='blog-home'),
    path('user/<str:username>/', UserAllNewsView.as_view(), name='user-news'),
    path('news/<int:pk>/', NewsDetailView.as_view(), name='news-detail'),
    path('news/add/', CreateNewsView.as_view(), name='news-create'),
    path('news/<int:pk>/update/', UpdateNewsView.as_view(), name='news-update'),
    path('news/<int:pk>/delete/', DeleteNewsView.as_view(), name='news-delete'),
    path('contacts/', contacts, name='blog-contacts')
]
