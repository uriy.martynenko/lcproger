from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class News(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:news-detail', kwargs={'pk': self.pk})
