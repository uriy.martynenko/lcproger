from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import Profile

if not Profile:
    @receiver(post_save, sender=User)
    def create_profile(sender, instance, created, **kwargs):
        user = instance
        if created:
            profile = Profile(user=user)
            profile.save()


    @receiver(post_save, sender=User)
    def update_profile(sender, instance, **kwargs):
        user = instance
        profile = Profile(user=user)
        profile.save()
