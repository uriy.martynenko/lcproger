from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect

from users.forms import UserRegistrationForm, ProfileImageForm, UserUpdateForm


@csrf_protect
def register(request: HttpRequest):
    if request.method == 'POST':
        creation_form = UserRegistrationForm(request.POST)

        if creation_form.is_valid():
            creation_form.save()
            username = creation_form.cleaned_data.get('username')
            messages.success(request,
                             f"User {username} has been registered, please enter your username and password to login.")
            return redirect('users:login-user')
    else:
        creation_form = UserRegistrationForm()
    return render(request, 'users/registration.html', {'creation_form': creation_form})


@login_required
def user_profile_editing(request):
    if request.method == 'POST':
        img_profile = ProfileImageForm(request.POST, request.FILES, instance=request.user.profile)
        update_user = UserUpdateForm(request.POST, instance=request.user)

        if img_profile.is_valid() and update_user.is_valid():
            img_profile.save()
            update_user.save()
            messages.success(request, f"Ваш профиль обновлен")
    else:
        img_profile = ProfileImageForm(instance=request.user.profile)
        update_user = UserUpdateForm(instance=request.user)
    data = {
        'img_profile': img_profile,
        'update_user': update_user,
    }
    return render(request, 'users/pofile.html', data)
