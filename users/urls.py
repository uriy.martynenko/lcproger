from django.contrib.auth import views
from django.urls import path

from users.views import register, user_profile_editing

app_name = 'users'

urlpatterns = [
    path('', register, name='register-user'),
    path('login/', views.LoginView.as_view(template_name='users/login.html'), name='login-user'),
    # path('reset_password/', views.PasswordResetView.as_view(template_name='users/pass_reset.html'), name='pass-reset'),
    # path('password_reset/done/>',
    #      views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'),
    #      name='password_reset_done'),
    # path('reset_password/password_reset_confirm/<uidb64>/<token>/',
    #      views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'),
    #      name='password_reset_confirm'),

    path('logout/', views.LogoutView.as_view(template_name='users/logout.html'), name='logout-user'),
    path('profile/', user_profile_editing, name='profile'),
]
